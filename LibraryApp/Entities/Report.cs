﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Entities
{
    public class Report
    {
        public string AuthorName { get; set; }

        public string Date { get; set; }
        public string ReportText {get; set;} 

    }
}